### Инструкция по запуску

1. `docker-compose up -d --build`
2. `docker exec -it nodetest_web_1 sh`
3. `cd app`
4. `npm init vue`
5. Ответить "No" на всё, кроме Vue Router
6. `cd {название проекта}`
7. `npm i`
8. `vite --host --port 8000`
9. Перейти по адресу [127.0.0.1:8000](127.0.0.1:8000)
10. ???
11. profit!