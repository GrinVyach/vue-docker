FROM nginx:alpine
#FROM node:current-alpine3.15

RUN apk add wget
RUN apk add nano
RUN apk add nodejs npm
RUN apk add yarn

RUN yarn global add @vue/cli @vue/cli-service-global create-vue vite

EXPOSE 80
EXPOSE 8000